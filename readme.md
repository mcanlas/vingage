### Vingage
___

Vingage was created for students to take notes during video lectures.
Adding engagment and providing a better learning experience. 

[Vingage Demo](https://vingage.herokuapp.com)
If you want to demo vingage please use these credentials to log in:

> *Username*: employer
> *Password*: saltedpass

*Some functionality has been temporarily disabled*

Listed below are the technologies used:

- BackboneJS
- NodeJS  
- Jade  
- Express  
- Mongo  
- Heroku  
- AWS S3
